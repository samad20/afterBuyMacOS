# afterBuyMacOS
******************

![Launch - screenshot ](https://gitlab.com/samad20/afterBuyMacOS/-/raw/main/afterBuyMacOS.png)

******************
 My personal script after buy or reset MacOS
 
 To use this script copy these command to Terminal:

 1. Download
```
curl https://gitlab.com/samad20/afterBuyMacOS/-/raw/main/afterBuyMacOS.sh --output afterBuyMacOS.sh
```
 2. edit it if you want, then execute:
```
chmod +x afterBuyMacOS.sh
```
 3. Run
```
./afterBuyMacOS.sh
```
